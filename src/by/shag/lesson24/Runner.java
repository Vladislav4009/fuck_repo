package by.shag.lesson24;

import java.io.*;

public class Runner {
    public static void main(String[] args) {

        String fileName = "lesson24_01.txt";
        String dirName = System.getProperty("user.dir");
        String fullName = dirName + File.separator + fileName;

        System.out.println("File path : " + fullName);

        File file = new File(fullName);

        if (!file.exists()) {
            try {
                if (file.createNewFile()) {
                    System.out.println("File created");
                } else {
                    System.out.println("Something wrong");
                }
            } catch (IOException e) {
                System.out.println("Wrong logic for io");
            }
        } else {
            System.out.println("File already exist!");
        }


        String dirname = dirName + "/tmp";
        File directory = new File(dirname);
        directory.mkdirs();

        if (directory.isDirectory()) {
            System.out.println("Уже создана");
        }

//        file.deleteOnExit();
//        file.getFreeSpace();
//        file.getTotalSpace();
//        file.setReadOnly();

//        readAndWriteInOnePortion();
//        readAndWriteInPortions();
//        readAndWriteInBytes();
//        readAndWriteByteArray();
//        readAndWriteBufferedStreams();
//        readerAndWriter();
    }

    private static void readAndWriteInOnePortion() {
        InputStream in = null;

        OutputStream out = null;
        byte[] buffer = null;
        try {
            String fileName = "lesson24_01.txt";
            String dirName = System.getProperty("user.dir");
            String fullName = dirName + File.separator + fileName;
            System.out.println("File path : " + fullName);

            File inputFile = new File(fullName);
            in = new FileInputStream(inputFile);
            buffer = new byte[in.available()];
            long startTime = System.currentTimeMillis();
            in.read(buffer);

            String fileOutputName = "lesson24_02.txt";
            String dirOutputName = System.getProperty("user.dir");
            String fullOutputName = dirOutputName + File.separator + fileOutputName;
            System.out.println("File path : " + fullOutputName);

            out = new FileOutputStream(fullOutputName);
            out.write(buffer);
            long finishTime = System.currentTimeMillis();
            System.out.println(finishTime - startTime);
        } catch (FileNotFoundException e) {
            System.out.println("file not found");
        } catch (IOException e) {
            System.out.println("problem with io logic");
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                System.out.println("exception during the close input stream");
            }
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                System.out.println("exception during the close output stream");
            }
        }
    }

    private static void readAndWriteInPortions() {
        String fileInputName = "1.png";
        String dirInputName = System.getProperty("user.dir");
        String fullInputName = dirInputName + File.separator + fileInputName;
        String fileOutputName = "2.png";
        String dirOutputName = System.getProperty("user.dir");
        String fullOutputName = dirOutputName + File.separator + fileOutputName;

        byte[] buffer = new byte[8 * 1024];

        try (InputStream in = new FileInputStream(fullInputName);
             OutputStream out = new FileOutputStream(fullOutputName)) {

            int bytesRead = 0;
            while ((bytesRead = in.read(buffer)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
        } catch (FileNotFoundException e) {
            System.out.println("file not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("exception during the close input stream");
            e.printStackTrace();
        }
    }

    private static void readAndWriteInBytes() {
        String fileInputName = "11.txt";
        String dirInputName = System.getProperty("user.dir");
        String fullInputName = dirInputName + File.separator + fileInputName;
        String fileOutputName = "22.txt";
        String dirOutputName = System.getProperty("user.dir");
        String fullOutputName = dirOutputName + File.separator + fileOutputName;

        try (InputStream in = new FileInputStream(fullInputName);
             OutputStream out = new FileOutputStream(fullOutputName)) {
            int oneByte;
            while ((oneByte = in.read()) != -1) {
                if (oneByte < 65) {
                    out.write(oneByte);
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("file not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("exception during the close input stream");
            e.printStackTrace();
        }
    }

    private static void readAndWriteByteArray() {
        String line = "This is a sample string";
        ByteArrayInputStream bais = new ByteArrayInputStream(line.getBytes());
        int ch;
        StringBuilder sb = new StringBuilder();
        while ((ch = bais.read()) != -1) {
            sb.append(Character.toUpperCase((char) ch));
        }
        System.out.println("Capitalized string " + sb.toString());
    }

    private static void readAndWriteBufferedStreams() {
        String line = "This is 25 lesson";

        try (FileOutputStream out = new FileOutputStream("buffered_stream.txt");
        BufferedOutputStream bos = new BufferedOutputStream(out)){

            byte[] lineInBytes = line.getBytes();
            bos.write(lineInBytes);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (FileInputStream in = new FileInputStream("buffered_stream.txt");
        BufferedInputStream bis = new BufferedInputStream(in)) {
            int c;
            while ((c = bis.read()) != -1) {
                System.out.print((char) c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void readerAndWriter() {
        try (FileReader in = new FileReader("file_reader.txt");
        BufferedReader br = new BufferedReader(in);
        FileWriter out = new FileWriter("file_reader_out.txt", true);
        BufferedWriter bw = new BufferedWriter(out)) {

            String line = null;
            int lineCounter = 0;

            while ((line = br.readLine()) != null) {
                if ((++lineCounter) % 2 == 0) {
                    String newLine = line.replace("e", "E");
                    bw.write(newLine + System.getProperty("line.separator"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
