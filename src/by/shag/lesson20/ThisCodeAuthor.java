package by.shag.lesson20;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.TYPE})
@Documented
public @interface ThisCodeAuthor {

    String author();
    String creationDate();
    int version();
}
