package by.shag.lesson20;

import java.util.Comparator;

public class HumanLastNameComparator implements Comparator<Human> {

    @Override
    public int compare(Human o1, Human o2) {
        return -1 * o1.getLastName().length() - o2.getLastName().length();
    }
}
