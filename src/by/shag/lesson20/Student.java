package by.shag.lesson20;

import java.util.Comparator;

public class Student implements Comparable<Student>{

    private String name;

    public String getName() {
        return name;
    }

    public Student(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Student student) {
       return this.name.compareTo(student.getName()) * -1;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                '}';
    }
}
