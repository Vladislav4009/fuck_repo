package by.shag.lesson20;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Runner {

    public static void main(String[] args) {

        Set<Student> students = new TreeSet<>();

//        Student dima = new Student("Dima");
//        Student vlad = new Student("Vlad");
//        Student aleg = new Student("Aleg");
//        Student anton = new Student("Anton");
//        Student dimaa = new Student("Dima");
//
//        students.add(dima);
//        students.add(vlad);
//        students.add(aleg);
//        students.add(anton);
//        students.add(dimaa);

//        System.out.println(students);

//
//        Set<Person> person = new TreeSet<>();

//        Person person1 = new Person("Aaa", "Rrr");
//        Person human2 = new Person("Bbb", "Ttt");
//        Person human3 = new Person("Ccc", "Ttt");
//        Person human4 = new Person("Ddd", "Jjj");
//        Person human5 = new Person("Aaa", "Rrr");
//
//        person.add(person1);
//        person.add(human2);
//        person.add(human3);
//        person.add(human4);
//        person.add(human5);
//
//        System.out.println(person);

        Set<Human> humans = new TreeSet<>(new HumanLastNameComparator().reversed()
                .thenComparing(new HumanAgeComparator()));

        Human human1 = new Human("Aaa", "Rrrrrrrrr", 45);
        Human human2 = new Human("Bbbbb", "Ttttt", 12);
        Human human3 = new Human("Cccccc", "Tttt", 5);
        Human human4 = new Human("Ddddddd", "Jjjjjj", 5);
        Human human5 = new Human("Aaaa", "Jjjjjj", 5);

        humans.add(human1);
        humans.add(human2);
        humans.add(human3);
        humans.add(human4);
        humans.add(human5);

        System.out.println(humans);

    }
}
