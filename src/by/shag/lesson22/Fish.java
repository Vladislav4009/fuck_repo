package by.shag.lesson22;

public class Fish {

    private String name;
    private double price;
    private boolean isFresh;

    public Fish(String name, double price, boolean isFresh) {
        this.name = name;
        this.price = price;
        this.isFresh = isFresh;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isFresh() {
        return isFresh;
    }

    public void setFresh(boolean fresh) {
        isFresh = fresh;
    }

    @Override
    public String toString() {
        return "Fish{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", isFresh=" + isFresh +
                '}';
    }
}
