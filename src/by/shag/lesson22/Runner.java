package by.shag.lesson22;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Runner {
    public static void main(String[] args) {
        List<Fish> fishes = new ArrayList<>();
        Fish fish1 = new Fish("karp", 12.12, true);
        Fish fish2 = new Fish("semga", 12.22, true);
        Fish fish3 = new Fish("polosatic", 12.62, false);

        fishes.add(fish1);
        fishes.add(fish2);
        fishes.add(fish3);

        Comparator<Fish> nameComparator = new Comparator<Fish>() {
            @Override
            public int compare(Fish o1, Fish o2) {
                return o1.getName().compareTo(o2.getName());
            }
        };

        fishes.sort(nameComparator);
        System.out.println(fishes);

        lambdaMethod();
    }

    public static void lambdaMethod() {

        Flyable bird = new Flyable() {

            @Override
            public void fly() {
                System.out.println("I am bird");
            }
        };

        Flyable bird2 = () -> System.out.println("I am bird2");

        bird.fly();
        bird2.fly();
    }
}
