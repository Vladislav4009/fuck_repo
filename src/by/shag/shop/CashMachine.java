package by.shag.shop;

import java.util.HashMap;
import java.util.Map;

public class CashMachine {

    private final Map<Integer, Integer> pricesByCode = new HashMap<>();

    public CashMachine() {
    }

    public Map<Integer, Integer> getPricesByCode() {
        return pricesByCode;
    }

    public void addProductPrice(Integer code, Integer priceInCents) {
        pricesByCode.put(code, priceInCents);
    }
}
