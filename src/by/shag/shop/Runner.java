package by.shag.shop;

import java.time.LocalDate;

public class Runner {
    public static void main(String[] args) {

        Milk milk1 = new Milk();

        milk1.setBrandName("Milk1");
        milk1.setTypeOfContainer(TypeOfContainer.BOTTLE);
        milk1.setCapacity(2.5);
        milk1.setBarcode(123411);
        milk1.setExpirationDate(LocalDate.of(2021, 2, 26));

        Milk milk2 = new Milk();

        milk2.setBrandName("Milk2");
        milk2.setTypeOfContainer(TypeOfContainer.BOTTLE);
        milk2.setCapacity(2.5);
        milk2.setBarcode(123411);
        milk2.setExpirationDate(LocalDate.of(2021, 3, 4));

        Milk milk3 = new Milk();

        milk3.setBrandName("Milk3");
        milk3.setTypeOfContainer(TypeOfContainer.BOTTLE);
        milk3.setCapacity(2.5);
        milk3.setBarcode(123411);
        milk3.setExpirationDate(LocalDate.of(2021, 3, 5));

        Milk milk4 = new Milk();

        milk4.setBrandName("Milk4");
        milk4.setTypeOfContainer(TypeOfContainer.BOTTLE);
        milk4.setCapacity(2.5);
        milk4.setBarcode(123411);
        milk4.setExpirationDate(LocalDate.of(2021, 2, 27));

        Milk milk5 = new Milk();

        milk5.setBrandName("Milk5");
        milk5.setTypeOfContainer(TypeOfContainer.BOTTLE);
        milk5.setCapacity(2.5);
        milk5.setBarcode(123411);
        milk5.setExpirationDate(LocalDate.of(2021, 4, 5));

        Bread bread1 = new Bread();

        bread1.setName("Bread1");
        bread1.setBarcode(123421);
        bread1.setExpirationDate(LocalDate.of(2021, 3, 3));
        bread1.setSliced(true);
        bread1.setWeightInGramm(255);

        Bread bread2 = new Bread();

        bread2.setName("Bread2");
        bread2.setBarcode(123421);
        bread2.setExpirationDate(LocalDate.of(2021, 2, 26));
        bread2.setSliced(true);
        bread2.setWeightInGramm(255);

        Bread bread3 = new Bread();

        bread3.setName("Bread3");
        bread3.setBarcode(123421);
        bread3.setExpirationDate(LocalDate.of(2021, 2, 27));
        bread3.setSliced(true);
        bread3.setWeightInGramm(255);

        Bread bread4 = new Bread();

        bread4.setName("Bread4");
        bread4.setBarcode(123421);
        bread4.setExpirationDate(LocalDate.of(2021, 3, 26));
        bread4.setSliced(true);
        bread4.setWeightInGramm(255);

        Bread bread5 = new Bread();

        bread5.setName("Bread5");
        bread5.setBarcode(123421);
        bread5.setExpirationDate(LocalDate.of(2021, 3, 15));
        bread5.setSliced(true);
        bread5.setWeightInGramm(255);

        CashMachine cashMachine = new CashMachine();

        cashMachine.addProductPrice(123411, 100);
        cashMachine.addProductPrice(123421, 50);

        VillageShop villageShop = new VillageShop();

        CleverSeller cleverSeller = new CleverSeller("Продавец", villageShop, cashMachine);

        cleverSeller.addProduct(milk1);
        cleverSeller.addProduct(milk2);
        cleverSeller.addProduct(milk3);
        cleverSeller.addProduct(milk4);
        cleverSeller.addProduct(milk5);

        cleverSeller.addProduct(bread1);
        cleverSeller.addProduct(bread2);
        cleverSeller.addProduct(bread3);
        cleverSeller.addProduct(bread4);
        cleverSeller.addProduct(bread5);

        Cart cart = new Cart();

        cart.addProductToCart(milk1);
        cart.addProductToCart(milk2);
        cart.addProductToCart(bread1);
        cart.addProductToCart(bread2);
        cart.addProductToCart(bread3);

        cleverSeller.calculateTotalCost(cart);
    }
}
