package by.shag.shop;

public class CleverSeller {

    private final String name;
    private final VillageShop villageShop;
    private final CashMachine cashMachine;

    public CleverSeller(String name, VillageShop villageShop, CashMachine cashMachine) {
        this.name = name;
        this.villageShop = villageShop;
        this.cashMachine = cashMachine;
    }

    @Override
    public String toString() {
        return "CleverSeller{" +
                "name='" + name + '\'' +
                ", villageShop=" + villageShop +
                ", cashMachine=" + cashMachine +
                '}';
    }

    public void addProduct(Product product) {
        if (product instanceof Milk) {
            Milk milk = (Milk) product;
            villageShop.addMilkToShell(milk);
        }
        if (product instanceof Bread) {
            Bread bread = (Bread) product;
            villageShop.addBreadToShell(bread);
        }
    }

    public void getMilkFromShell() {
        if (!villageShop.getMilkShell().isEmpty()) {
            if (villageShop.getMilkShell().element().isExpired()) {
                System.out.println("Простите, не заметил. Сча дам другое");
                villageShop.getMilkShell().remove();
                getMilkFromShell();
            } else {
                villageShop.getMilkShell().poll();
            }
        } else {
            System.out.println("Сори, закончилось.");
        }
    }

    public void getBreadFromShell() {
        if (!villageShop.getBreadShell().isEmpty()) {
            if (villageShop.getBreadShell().element().isExpired()) {
                System.out.println("Простите, не заметил. Сча дам другое");
                villageShop.getBreadShell().remove();
                getBreadFromShell();
            } else {
                villageShop.getBreadShell().poll();
            }
        } else {
            System.out.println("Сори, закончилось.");
        }
    }

    public void calculateTotalCost(Cart cart) {
        if (cart.getProducts().isEmpty()) {
            System.out.println("корзина пуста");
        } else {
            for (int i = 0; i < cart.getProducts().size(); i++) {
                if (cart.getProducts().get(i) instanceof Milk) {
                    getMilkFromShell();
                }
                if (cart.getProducts().get(i) instanceof Bread) {
                    getBreadFromShell();
                }
            }
            int result = 0;
            for (int i = 0; i < cart.getProducts().size(); i++) {
                result += cashMachine.getPricesByCode().get(cart.getProducts().get(i).getBarcode());
            }
            System.out.println(result);
        }
    }
}
