package by.shag.shop;

public enum TypeOfContainer {

    PACKAGE,
    TETRAPAK,
    BOTTLE

}
