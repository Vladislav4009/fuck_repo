package by.shag.shop;

import java.time.LocalDate;

public class Bread extends Product {

    private Boolean isSliced;
    private Integer weightInGramm;

    public Bread() {
    }

    public boolean isSliced() {
        return isSliced;
    }

    public void setSliced(boolean sliced) {
        isSliced = sliced;
    }

    public int getWeightInGramm() {
        return weightInGramm;
    }

    public void setWeightInGramm(int weightInGramm) {
        this.weightInGramm = weightInGramm;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public Integer getBarcode() {
        return super.getBarcode();
    }

    @Override
    public LocalDate getExpirationDate() {
        return super.getExpirationDate();
    }

    @Override
    public String toString() {
        return super.toString() + String.format(", нарезанный - %s, вес в граммах - %s", isSliced, weightInGramm);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof Bread)) {
            return false;
        }
        Bread bread = (Bread) o;
        if (this == o) {
            return true;
        }
        return (this.getName() == null ?
                bread.getName() == null : this.getName().equals(bread.getName())) &&
                (this.getBarcode().equals(bread.getBarcode())) && (this.getExpirationDate().equals(bread.getExpirationDate())) &&
                (this.isSliced == bread.isSliced) && (this.weightInGramm.equals(bread.weightInGramm));
    }

    @Override
    public int hashCode() {
        int hashCode = 17;
        hashCode = 31 * hashCode + (getName() == null ? 0 : getName().hashCode());
        hashCode = 31 * hashCode + getBarcode();
        hashCode = 31 * hashCode + getExpirationDate().hashCode();
        hashCode = 31 * hashCode + isSliced.hashCode();
        hashCode = 31 * hashCode + weightInGramm;
        return hashCode;
    }

    public int compareTo(Bread bread) {
        if (getExpirationDate().equals(bread.getExpirationDate())) {
            return 0;
        } else if (getExpirationDate().isBefore(bread.getExpirationDate())) {
            return -1;
        } else {
            return 1;
        }
    }
}
