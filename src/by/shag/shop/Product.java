package by.shag.shop;

import java.time.LocalDate;

public abstract class Product implements Comparable<Product> {

    private String name;
    private Integer barcode;
    private LocalDate expirationDate;

    public Product() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBarcode() {
        return barcode;
    }

    public void setBarcode(Integer barcode) {
        this.barcode = barcode;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {
        return String.format("Имя продукта - %s, штрихкод - %s, срок годности - %s", name, barcode, expirationDate);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof Product)) {
            return false;
        }
        Product product = (Product) o;
        if (this == o) {
            return true;
        }
        return barcode.equals(product.barcode);
    }

    @Override
    public int hashCode() {
        int hashCode = 17;
        return 31 * hashCode + barcode;
    }

    @Override
    public int compareTo(Product product) {
        if (getExpirationDate().equals(product.getExpirationDate())) {
            return 0;
        } else if (getExpirationDate().isBefore(product.getExpirationDate())) {
            return -1;
        } else {
            return 1;
        }
    }

    public boolean isExpired() {
        if (this.expirationDate.isEqual(LocalDate.now())) {
            return true;
        } else return this.expirationDate.isBefore(LocalDate.now());
    }
}
