package by.shag.shop;

import java.util.Objects;

public class Milk extends Product {

    private String brandName;
    private TypeOfContainer typeOfContainer;
    private Double capacity;

    public Milk() {
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public TypeOfContainer getTypeOfContainer() {
        return typeOfContainer;
    }

    public void setTypeOfContainer(TypeOfContainer typeOfContainer) {
        this.typeOfContainer = typeOfContainer;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    @Override
    public Integer getBarcode() {
        return super.getBarcode();
    }

    @Override
    public String toString() {
        return super.toString() + String.format(", бренд - %s, тип тары - %s, обьем - %s", brandName, typeOfContainer, capacity);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof Milk)) {
            return false;
        }
        Milk milk = (Milk) o;
        if (this == o) {
            return true;
        }
        return Objects.equals(this.getBarcode(), milk.getBarcode());
    }

    @Override
    public int hashCode() {
        int hashCode = 17;
        return 31 * hashCode + getBarcode();
    }

    public int compareTo(Milk milk) {
        if (getExpirationDate().equals(milk.getExpirationDate())) {
            return 0;
        } else if (getExpirationDate().isBefore(milk.getExpirationDate())) {
            return -1;
        } else {
            return 1;
        }
    }
}
