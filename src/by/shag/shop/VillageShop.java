package by.shag.shop;

import java.util.PriorityQueue;
import java.util.Queue;

public class VillageShop {

    private Queue<Milk> milkShell = new PriorityQueue<>();
    private Queue<Bread> breadShell = new PriorityQueue<>();

    public VillageShop() {
    }

    public Queue<Milk> getMilkShell() {
        return milkShell;
    }

    public Queue<Bread> getBreadShell() {
        return breadShell;
    }

    @Override
    public String toString() {
        return "VillageShop{" +
                "milkShell=" + milkShell +
                ", breadShell=" + breadShell +
                '}';
    }

    public void addMilkToShell(Milk milk) {
        milkShell.add(milk);
    }

    public void addBreadToShell(Bread bread) {
        breadShell.add(bread);
    }

}
