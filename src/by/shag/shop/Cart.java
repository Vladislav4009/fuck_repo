package by.shag.shop;

import java.util.ArrayList;
import java.util.List;

public class Cart {

    private final List<Product> products = new ArrayList<>();

    public Cart() {
    }

    public List<Product> getProducts() {
        return products;
    }

    public void addProductToCart(Product product) {
        products.add(product);
    }
}
