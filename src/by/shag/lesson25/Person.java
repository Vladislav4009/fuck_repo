package by.shag.lesson25;

import java.io.*;
import java.util.Base64;

public class Person implements Externalizable {

    private static int count = 0;

    private String lastName;
    private String name;
    private transient String password;
    private Passport passport = new Passport("5245");
    private int a;

    public Person(String name, String lastName, String password) {
        count++;
        this.name = name;
        this.lastName = lastName;
        this.password = password;
    }

    public Person() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", passport=" + passport +
                '}';
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(lastName);
        out.writeObject(name);
        out.writeObject(encryptString(password));
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        this.lastName = (String) in.readObject();
        this.name = (String) in.readObject();
        this.password = decryptString((String) in.readObject());
    }

    private String encryptString(String data) {
        String encryptData = Base64.getEncoder().encodeToString(data.getBytes());
        System.out.println(encryptData);
        return encryptData;
    }

    private String decryptString(String data) {
        String decrypted = new String(Base64.getDecoder().decode(data));
        System.out.println(decrypted);
        return decrypted;
    }
}
