package by.shag.lesson25;

import java.io.Serializable;

public class Passport implements Serializable {

    private String ser;

    public Passport(String ser) {
        this.ser = ser;
    }

    @Override
    public String toString() {
        return "Passport{" +
                "ser='" + ser + '\'' +
                '}';
    }
}
