package by.shag.lesson25;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Runner {
    public static void main(String[] args) {

        Person person1 = new Person("Sanya", "Pushkin", "1234");

        try (FileOutputStream fout = new FileOutputStream("person.txt");
             ObjectOutputStream oout = new ObjectOutputStream(fout)) {
            oout.writeObject(person1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (FileInputStream fis = new FileInputStream("person.txt");
             ObjectInputStream ois = new ObjectInputStream(fis)){
            Person deserializedPerson = (Person) ois.readObject();
            System.out.println(deserializedPerson);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
