package by.shag.lesson30;

public class LoginService {

    private static final String EMAIL = "secret@gmail.com";
    private static final String PASSWORD = "QwErTy";

    // Cases:
    // 1. happy path
    // 2. If any value is null (two cases)
    // 3. If any value is empty (two cases)
    // 4. If any value is not equals with original (two cases)
    // 5. If email in uppercase (one case)
    // 6. If password in uppercase (one case)
    public boolean login(String email, String password) {
        if (email == null || password == null) {
            throw new IllegalArgumentException();
        }

        if (email.isEmpty() || password.isEmpty()) {
            throw new IllegalArgumentException();
        }

        if (password.equals(PASSWORD) && email.equalsIgnoreCase(EMAIL)) { // !!!
            return true;
        }

        return false;
    }
}
