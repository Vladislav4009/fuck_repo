package by.shag.lesson30;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

class LoginServiceTest {

    private LoginService loginService;

    @BeforeEach
    void setUp() {
        loginService = new LoginService();
    }

    @Test
    void happyPathLoginCheck() {
        String validPassword = "QwErTy";
        String validEmail = "secret@gmail.com";

        boolean result = loginService.login(validEmail, validPassword);

        assertTrue(result);
    }

    @Test
    void ifPasswordValueIsNullLoginCheck() {
        String validPassword = null;
        String validEmail = "secret@gmail.com";

        assertThrows(IllegalArgumentException.class, () -> loginService.login(validEmail, validPassword));
    }

    @Test
    void ifEmailValueIsNullLoginCheck() {
        String validPassword = "QwErTy";
        String validEmail = null;

        assertThrows(IllegalArgumentException.class, () -> loginService.login(validEmail, validPassword));
    }

    @Test
    void ifEmailValueIsEmptyLoginCheck() {
        String validPassword = "QwErTy";
        String validEmail = "";

        assertThrows(IllegalArgumentException.class, () -> loginService.login(validEmail, validPassword));

    }

    @Test
    void ifPasswordValueIsEmptyLoginCheck() {
        String validPassword = "";
        String validEmail = "secret@gmail.com";

        assertThrows(IllegalArgumentException.class, () -> loginService.login(validEmail, validPassword));

    }

    @Test
    void ifEmailValueIsNotEqualsWithOriginalLoginCheck() {
        String validPassword = "QwErTy";
        String validEmail = "seret@gmail.com";

        assertFalse(loginService.login(validEmail, validPassword));

    }

    @Test
    void ifPasswordValueIsNotEqualsWithOriginalLoginCheck() {
        String validPassword = "QwErty";
        String validEmail = "secret@gmail.com";

        assertFalse(loginService.login(validEmail, validPassword));

    }

    @Test
    void ifEmailValueInUppercaseLoginCheck() {
        String validPassword = "QwErTy";
        String validEmail = "SECRET@gmail.com";

        assertTrue(loginService.login(validEmail, validPassword));

    }

    @Test
    void ifPasswordValueInUppercaseLoginCheck() {
        String validPassword = "QWERTY";
        String validEmail = "SECRET@gmail.com";

        assertFalse(loginService.login(validEmail, validPassword));

    }
}