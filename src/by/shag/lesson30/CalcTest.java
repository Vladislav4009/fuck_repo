package by.shag.lesson30;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CalcTest {

    @BeforeAll
    static void beforeAll() {
        System.out.println("Перед тестовым классам");
    }

    @AfterAll
    static void afterAll() {
        System.out.println("После класса");
    }

    @BeforeEach
    void setUp() {
        System.out.println("Перед каждым тестом");
    }

    @AfterEach
    void teardown() {
        System.out.println("После каждого");
    }

    @Test
    @Order(1)
    void tesSum() {
        int a = 5;
        int b = 7;
        int result = Calc.sum(a, b);
        assertEquals(12, result);
        System.out.println(1);
    }

    @Test
    @Order(3)
    void sum2() {
        int a = 6;
        int b = 7;
        int result = Calc.sum(a, b);
        assertEquals(13, result);
        System.out.println(2);
    }

    @Order(2)
    @Test
    void divide() {
        int a = 5;
        int b = 2;
        int result = Calc.divide(a, b);
        assertEquals(2, result);
    }

    @Order(4)
    @Test
    void divideByZero() {
        int a = 5;
        int b = 0;
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> Calc.divide(a, b));
        assertEquals("Нельзя делить на 0", exception.getMessage());
    }

}