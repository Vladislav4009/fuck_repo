package by.shag.lesson36;

import by.shag.lesson35.ConnectionCreator;

import java.sql.*;

public class    StudentJ2020Service {

    private final String PREPARED_INSERT =
            "insert into j2020(name, last_name, phone_number) values (?, ?, ?) on conflict do nothing";
    private ConnectionCreator creator = new ConnectionCreator();

    public StudentJ2020 create(StudentJ2020 student) throws Exception {

        Connection ourConnection = null;
        try (Connection connection = creator.createConnection();
             PreparedStatement statement = connection.prepareStatement(PREPARED_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            ourConnection = connection;
            connection.setAutoCommit(false);
            statement.setString(1, student.getName());
            statement.setString(2, student.getLastName());
            statement.setString(3, student.getPhoneNumber());
            statement.executeUpdate();
            connection.commit();
            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                int key = generatedKeys.getInt(1);
                student.setId(key);
            }

        } catch (SQLException e) {
            if (ourConnection != null) {
                ourConnection.rollback();
            }
            throw new StudentJ2020Exception(e);
        }
        return student;
    }
}
