package by.shag.lesson21;

public class LimitedBicycle {
    public static int count;
    private String model;
    private String color;
    private int weight;

    public LimitedBicycle(String model, String color, int weight) {
        this.model = model;
        this.color = color;
        this.weight = weight;
        count++;
    }

    public String getDesc() {

        class Gag {
            private int count;

            public int getCount() {
                return count;
            }

            public void setCount(int count) {
                this.count = count;
            }
        }
        Gag gag = new Gag();

        return String.format("Desc");
    }

    public static class LimitedHandleBar {

        private String name;
        private String color;

        public LimitedHandleBar(String name, String color) {
            this.name = name;
            this.color = color;
        }

        public void left() {
            System.out.println("Руль влево");
        }

        public void right() {
            System.out.println("Руль вправо");
        }
    }

    public class Seat {

        private String color;
        private int weight;

        public Seat(String color, int weight) {
            this.color = color;
            this.weight = weight;
        }

        public void up() {
            System.out.println("Поднял");
        }

        public void down() {
            System.out.println("Опустил");
        }

    }

}
