package by.shag.lesson35;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MonthsService {

    private ConnectionCreator creator = new ConnectionCreator();
    private final String PREPARED_INSERT = "insert into months(id, name, days) values (?, ?, ?)";

    public List<Month> getAllMonths() {
        List<Month> months = new ArrayList<>();
        try (Connection connection = creator.createConnection();
             Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
             ResultSet resultSet = statement.executeQuery("select * from months ")) {
            while (resultSet.next()) {
                months.add(handleResultSetRecord(resultSet));
            }
        } catch (SQLException e) {

        }
        return months;
    }

    private Month handleResultSetRecord(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");

        if (name.equalsIgnoreCase("March")) {
            resultSet.updateInt("days", 35);
            resultSet.updateRow();
        }

        int days = resultSet.getInt("days");

        if (id == 1) {
            resultSet.moveToInsertRow();
            resultSet.updateInt("id", 998);
            resultSet.updateString("name", "Lucky");
            resultSet.updateInt("days", 777);
            resultSet.insertRow();
            resultSet.moveToCurrentRow();
        }
        return new Month(id, name, days);
    }

    public void create(Month month) {
        try (Connection connection = creator.createConnection();
             Statement statement = connection.createStatement(
                     ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE)) {

                Integer id = month.getId();
                String name = month.getName();
                Integer days = month.getDays();
                String sql = "insert into months(id, days, name ) values (%s, %s, '%s')";
                sql = String.format(sql, id, days, name);
                statement.addBatch(sql);
                statement.executeBatch();

        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void createWithPrepareStatement(Month month) {

        try (Connection connection = creator.createConnection();
             PreparedStatement statement = connection.prepareStatement(PREPARED_INSERT)) {

            Integer id = month.getId();
            String name = month.getName();
            Integer days = month.getDays();

            statement.setInt(1, id);
            statement.setString(2, name);
            statement.setInt(3, days);
            statement.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
    }
}
