package by.shag.lesson35;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Runner {
    public static void main(String[] args) {

//        try {
//            Connection connection = DriverManager.getConnection(
//                    "jdbc:postgresql://localhost:5432/shag", "shag", "shag");
//            System.out.println("fuck");
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }

//        Properties properties = new Properties();
//        properties.put("user", "shag");
//        properties.put("password", "shag");
//        properties.put("autoReconnect", "true");
//        properties.put("characterEncoding", "UTF-8");
//        properties.put("serverTimezone", "UTS");
//        String url = "jdbc:postgresql://localhost:5432/shag";
//        try {
//            Connection connection = DriverManager.getConnection(url, properties);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        System.out.println("fuck");

        Properties properties = new Properties();
        try {
            properties.load(new FileReader(generateFilePath("src/by/shag/lesson35/database.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String url = (String) properties.get("db.url");

        try {
            Connection connection = DriverManager.getConnection(url, properties);
            Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            ResultSet resultSet = statement.executeQuery("select * from months");
            while (resultSet.next()) {

                System.out.println(String.format("%s %s %s",
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("days")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static String generateFilePath(String filename) {
        String dirName = System.getProperty("user.dir");
        return dirName + File.separator + filename;
    }

}
