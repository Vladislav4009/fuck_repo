package by.shag.lesson31;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Runner {
    public static void main(String[] args) {

//        List<String> list = List.of("123", "abc", "def");
//
//        Optional<List> optionalList = Optional.of(list);
//        Optional<String> optionalString1 = findInCollection(list,
//                (String s) -> s.equalsIgnoreCase("ABC"));

//        System.out.println(optionalString1.orElseThrow(() -> new RuntimeException("NO such element")));
//        optionalString1.orElseGet(() -> {
//            System.out.println("12345");
//            return "123";
//        });
//        optionalString1.orElse(returnSmth());

//        findInCollection(list, (String s) -> s == null);
//
//        Optional<String> optional = Optional.empty();
//        optional.isEmpty();
//        optional.isPresent();
//        optional.get();
//        String result = optional.orElse(null);
//        optional.orElseThrow(() -> new RuntimeException("123"));
//        List<String> list = List.of("123", "abc", "def");
//        long size = list.stream().count();
//        Stream<String> stringStream = Stream.of("123", "abc", "def");
//        String[] array = {"123", "abc", "def"};
//        Stream<String> stream = Arrays.stream(array);
//        String mother = "mother";
//        IntStream chars = mother.chars();
//        Stream<Object> objectStream = Stream.builder()
//                .add(1)
//                .add(2)
//                .add(5)
//                .build();
//        Stream<Integer> integerStream = Stream.iterate(1, n -> n + 2);
//        Stream<Double> doubleStream = Stream.generate(() -> Math.random() * 100);
//        List<Integer> collect = List.of("123", "1234", "12345", "3210", "23568", "23568", "90567", "98888", "9085670").stream()
//                .peek(System.out::println)
//                .filter(s -> s.length() < 7)
//                .skip(2)
//                .distinct()
//                .map(Integer::parseInt) // (s -> s.toUpperCase())
//                .limit(4)
//                .sorted((a, b) -> b - a)
//                .collect(Collectors.toList());
//        System.out.println(collect);
//        double average = List.of("1,23", "12,34", "1,2,3,4,5").stream()
//                .flatMap(s -> Arrays.stream(s.split(",")))
//                .mapToInt(Integer::parseInt)
//                .average()
//                .orElseGet(() -> 0);
//        System.out.println(average);
//
//        long max = Stream.of(
//                new HumanNew("Dima", 29, List.of(9, 10)),
//                new HumanNew("Anton", 40, List.of(8, 7, 4)),
//                new HumanNew("Maria", 18, List.of(2, 3, 4, 1)))
//                .flatMapToInt(humanNew -> humanNew.getMarks().stream().mapToInt(mark -> mark))
//                .max().orElseGet(() -> 0);
//        System.out.println(max);

//        Optional<HumanNew> first = Stream.of(
//                new HumanNew("Dima", 29, List.of(9, 10)),
//                new HumanNew("Anton", 40, List.of(8, 7, 4)),
//                new HumanNew("Maria", 18, List.of(2, 3, 4, 1)),
//                new HumanNew("Anton", 18, List.of(9, 9, 4, 1)))
//                .findFirst();
//                .findAny();
//        System.out.println(first.orElse(new HumanNew("Valentina", 10, new ArrayList<>())));

//        Stream.of(
//                new HumanNew("Dima", 29, List.of(9, 10)),
//                new HumanNew("Anton", 40, List.of(8, 7, 4)),
//                new HumanNew("Maria", 18, List.of(2, 3, 4, 1)),
//                new HumanNew("Anton", 18, List.of(9, 9, 4, 1)))
//                .collect(Collectors.toSet());
//                .count();
//                .anyMatch(humanNew -> humanNew.getName().equalsIgnoreCase("maria"))
//                .allMatch(humanNew -> !humanNew.getMarks().isEmpty());
//                .noneMatch(humanNew -> !humanNew.getMarks().isEmpty());
//                .max((h1, h2) -> h1.getAge() - h2.getAge());
//                .min((h1, h2) -> h1.getAge() - h2.getAge());
//                .forEach(humanNew -> System.out.println(humanNew));
//                .forEachOrdered(humanNew -> System.out.println(humanNew));
//                .toArray();
//                .map(HumanNew::getAge)
//                .reduce((a1, a2) -> a1 + a2);
        List<String> collection = List.of("a1", "a2", "a3", "a1");

//        entersQuantity(collection);
//        returnFirstElementOrZero(collection);
//        returnLastElementOrEmpty(collection);
//        System.out.println(returnThirdElementOrThrow(collection));
//        returnThirdElementInOrder(collection);
//        System.out.println(isElementExistInColl(collection));
//        System.out.println(isAllElementContainsNumberOne(collection));
//        System.out.println(addStringToAllElementAndCollectToList(collection));
//        System.out.println(deleteFirstSymbolAndCollectToMassive(collection));

        List<String> collectionTwo = List.of("1,2,3", "45,667");

        System.out.println(collectAllNumberToSet(collectionTwo));

    }

    private static Optional<String> findInCollection(List<String> list, Predicate<String> predicate) {
        for (String element : list) {
            if (predicate.test(element)) {
                return Optional.of(element);
            }
        }
        return Optional.empty();
    }

    private static String returnSmth() {
        System.out.println("or else");
        return "123";
    }

    private static void entersQuantity(List<String> collection) {

        long count = collection.stream()
                .filter(x -> x.equals("a1"))
                .count();
        System.out.println(count);
    }

    private static String returnFirstElementOrZero(List<String> collection) {

        return collection.stream()
                .findFirst()
                .orElse("0");
    }

    private static String returnLastElementOrEmpty(List<String> collection) {

        return collection.stream()
                .skip(collection.size() - 1)
                .findFirst()
                .orElse("empty");
    }

    private static String returnThirdElementOrThrow(List<String> collection) {

        return collection.stream()
                .filter(x -> x.equals("a3"))
                .findFirst()
                .orElseThrow(() -> new RuntimeException());
    }

    private static String returnThirdElementInOrder(List<String> collection) {

        return collection.stream()
                .skip(2)
                .findFirst()
                .orElse("Nothing");
    }

    private static List<String> returnTwoElementButStartFromTheSecond(List<String> collection) {

        return collection.stream()
                .skip(1)
                .limit(2)
                .collect(Collectors.toList());
    }

    private static List<String> returnAllElementsFromTemplate(List<String> collection) {

        return collection.stream()
                .filter(x -> x.contains("1"))
                .collect(Collectors.toList());
    }

    private static boolean isExist(List<String> collection) {

        return collection.stream()
                .anyMatch(x -> x.contains("a1"));
    }

    private static boolean isExist2(List<String> collection) {

        return collection.stream()
                .anyMatch(x -> x.contains("a3"));
    }

    private static boolean isMatchSymbolOne(List<String> collection) {

        return collection.stream()
                .allMatch(x -> x.contains("1"));
    }

    private static boolean isElementExistInColl(List<String> collection) {

        return collection.stream()
                .noneMatch(x -> x.contains("a8"));
    }

    private static boolean isAllElementContainsNumberOne(List<String> collection) {

        return collection.stream()
                .allMatch(x -> x.contains("1"));
    }

    private static List addStringToAllElementAndCollectToList(List<String> collection) {

        return collection.stream()
                .map(x -> x + "_000")
                .collect(Collectors.toList());
    }

    private static int[] deleteFirstSymbolAndCollectToMassive(List<String> collection) {

        return collection.stream()
                .map(x -> x.substring(1))
                .mapToInt(Integer::parseInt)
                .toArray();
    }

    private static Set collectAllNumberToSet(List<String> collection) {

        return collection.stream()
                .flatMap((p) -> Arrays.stream(p.split(",")))
                .collect(Collectors.toSet());
    }



}
