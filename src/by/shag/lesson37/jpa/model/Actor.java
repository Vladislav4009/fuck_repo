package by.shag.lesson37.jpa.model;

import java.util.Date;
import java.util.Objects;

public class Actor {

    private Long id;
    private String name;
    private String lastName;
    private Date dateOfBirth;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Actor actor = (Actor) o;
        return Objects.equals(id, actor.id) &&
                Objects.equals(name, actor.name) &&
                Objects.equals(lastName, actor.lastName) &&
                Objects.equals(dateOfBirth, actor.dateOfBirth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, lastName, dateOfBirth);
    }
}
