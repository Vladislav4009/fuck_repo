package by.shag.lesson37.jpa.repository;

import by.shag.lesson37.exception.EntityRepositoryException;
import by.shag.lesson37.jpa.model.Actor;

import java.util.List;

public interface ActorRepository extends CRUDRepository<Actor, Long> {

    List<Actor> findByNameAndLastName(String name, String lastName) throws EntityRepositoryException;
}
