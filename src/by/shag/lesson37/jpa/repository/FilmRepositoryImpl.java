package by.shag.lesson37.jpa.repository;

import by.shag.lesson37.exception.EntityRepositoryException;
import by.shag.lesson37.jpa.model.Film;
import by.shag.lesson37.mapping.FilmMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FilmRepositoryImpl implements FilmRepository {

    private static final String FIND_BY_NAME = "select * from films f where f.name like ? ";
    private static final String FILM_SAVE = "insert into films (name, year, rating) values (?, ?, ?) ";
    private static final String FIND_BY_ID = "select * from films f where f.id = ? ";
    private static final String FIND_BY_ID_QUERY = "select * from films f where f.id = ? ";
    private static final String FIND_ALL_QUERY = "select * from films ";
    private static final String FILM_UPDATE = "update films f set f.name = ?, f.year = ?, f.rating = ? where f.id = ? ";
    private static final String DELETE_BY_ID = "delete from films f where f.id = ? ";

    private FilmMapper mapper = new FilmMapper();

    private Connection connection;

    @Override
    public List<Film> findByName(String name) throws EntityRepositoryException {
        try(PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_QUERY)) {
            preparedStatement.setString(1, name);
            ResultSet rs = preparedStatement.executeQuery();
            List<Film> result = new ArrayList<>();
            while (rs.next()) {
                result.add(mapper.map(rs));
            }
            return result;
        } catch (SQLException e) {
            throw new EntityRepositoryException("Exception in findByName method", e);
        }
    }

    @Override
    public Film save(Film entity) throws EntityRepositoryException {
        try (PreparedStatement statement = connection.prepareStatement(FILM_SAVE, Statement.RETURN_GENERATED_KEYS)) {
            mapper.populate(entity, statement);
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                entity.setId(rs.getInt("id"));
            }
            return entity;
        } catch (SQLException e) {
            throw new EntityRepositoryException("Exception is save metod", e);
        }
    }

    @Override
    public Optional<Film> findById(Integer integer) throws EntityRepositoryException {
        return Optional.empty();
    }

    @Override
    public List<Film> findAll() throws EntityRepositoryException {
        return null;
    }

    @Override
    public Film update(Film entity) throws EntityRepositoryException {
        return null;
    }

    @Override
    public void deleteById(Integer integer) throws EntityRepositoryException {

    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public void setConnection() {

    }

}
