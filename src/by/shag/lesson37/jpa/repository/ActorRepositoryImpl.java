package by.shag.lesson37.jpa.repository;

import by.shag.lesson37.exception.EntityRepositoryException;
import by.shag.lesson37.jpa.model.Actor;
import by.shag.lesson37.jpa.model.Film;
import by.shag.lesson37.mapping.ActorMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ActorRepositoryImpl implements ActorRepository {

    private static final String FIND_BY_NAME_AND_LAST_NAME = "select * from actors a where a.name like ? and a.last_name ? ";
    private static final String ACTOR_SAVE = "insert into actors (name, last_name, date_of_birth) values (?, ?, ?) ";
    private static final String FIND_BY_ID = "select * from actors a where a.id = ? ";
    private static final String FIND_BY_ID_QUERY = "select * from actors a where a.id = ? ";
    private static final String FIND_ALL_QUERY = "select * from actors ";
    private static final String ACTORS_UPDATE = "update actors a set a.name = ?, a.last_name = ?, a.date_of_birth = ? where a.id = ? ";
    private static final String DELETE_BY_ID = "delete from actors a where a.id = ? ";

    private ActorMapper mapper = new ActorMapper();

    private Connection connection;

    @Override
    public List<Actor> findByNameAndLastName(String name, String lastName) throws EntityRepositoryException {
        return null;
    }

    @Override
    public Actor save(Actor entity) throws EntityRepositoryException {
        try (PreparedStatement statement = connection.prepareStatement(ACTOR_SAVE, Statement.RETURN_GENERATED_KEYS)) {
            mapper.populate(entity, statement);
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                entity.setId(rs.getLong("id"));
            }
            return entity;
        } catch (SQLException e) {
            throw new EntityRepositoryException("Exception is save metod", e);
        }
    }

    @Override
    public Optional<Actor> findById(Long id) throws EntityRepositoryException {
        try(PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_QUERY)) {
            preparedStatement.setLong(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            Optional<Actor> actor = Optional.empty();
            if (rs.next()) {
                actor = Optional.of(mapper.map(rs));
            }
            return actor;
        } catch (SQLException e) {
            throw new EntityRepositoryException("Exception in findByName method", e);
        }
    }

    @Override
    public List<Actor> findAll() throws EntityRepositoryException {
        return null;
    }

    @Override
    public Actor update(Actor entity) throws EntityRepositoryException {
        return null;
    }

    @Override
    public void deleteById(Long aLong) throws EntityRepositoryException {

    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public void setConnection() {

    }
}
