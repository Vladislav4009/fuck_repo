package by.shag.lesson37.jpa.repository;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionCreator {

        private static String generateFilePath(String filename) {
            String dirName = System.getProperty("user.dir");
            return dirName + File.separator + filename;
        }

        public static Connection createConnection() {
            Properties properties = new Properties();
            try {
                properties.load(new FileReader(generateFilePath("src/by/shag/lesson35/database.properties")));
            } catch (IOException e) {
                throw new RuntimeException("Can not read connection properties");
            }
            String url = (String) properties.get("db.url");
            try {
                return DriverManager.getConnection(url, properties);
            } catch (SQLException e) {
                throw new RuntimeException("Impossible to create connection", e);
            }
        }
}