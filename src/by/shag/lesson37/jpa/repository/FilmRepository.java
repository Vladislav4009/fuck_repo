package by.shag.lesson37.jpa.repository;

import by.shag.lesson37.exception.EntityRepositoryException;
import by.shag.lesson37.jpa.model.Film;

import java.util.List;

public interface FilmRepository extends CRUDRepository<Film, Integer> {

    List<Film> findByName(String name) throws EntityRepositoryException;
}
