package by.shag.lesson37.jpa.transaction;

import by.shag.lesson37.exception.EntityRepositoryException;
import by.shag.lesson37.jpa.repository.CRUDRepository;
import by.shag.lesson37.jpa.repository.ConnectionCreator;

import java.sql.Connection;
import java.sql.SQLException;

public class EntityTransaction {

    private Connection connection;

    public void initTransaction(CRUDRepository... repositories) throws EntityRepositoryException {
        if (connection == null) {
            connection = ConnectionCreator.createConnection();
            try {
                connection.setAutoCommit(false);
            } catch (SQLException e) {
                throw new EntityRepositoryException("Impossible to disable autocommit", e);
            }
            for (CRUDRepository repo : repositories) {
                repo.setConnection();
            }
        }
    }

    public void endTransaction() throws EntityRepositoryException {
        try {
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new EntityRepositoryException("Impossible to disable autocommit", e);
        }
    }

    public void commit() throws EntityRepositoryException {
        try {
            connection.commit();
        } catch (SQLException e) {
            throw new EntityRepositoryException("Impossible to commit", e);
        }
    }

    public void rollback() throws EntityRepositoryException {
        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new EntityRepositoryException("Impossible to rollback", e);
        }
    }
}
