package by.shag.lesson37.service;

import by.shag.lesson37.exception.EntityRepositoryException;
import by.shag.lesson37.jpa.model.Actor;
import by.shag.lesson37.jpa.model.Film;
import by.shag.lesson37.jpa.repository.ActorRepository;
import by.shag.lesson37.jpa.repository.ActorRepositoryImpl;
import by.shag.lesson37.jpa.repository.FilmRepository;
import by.shag.lesson37.jpa.repository.FilmRepositoryImpl;
import by.shag.lesson37.jpa.transaction.EntityTransaction;

import java.util.List;

public class FilmService {

    private FilmRepository filmRepository = new FilmRepositoryImpl();
    private ActorRepository actorRepository = new ActorRepositoryImpl();

    public void saveWithActors(Film film, List<Actor> actors) {
        EntityTransaction tx = new EntityTransaction();
        try {
            tx.initTransaction(filmRepository, actorRepository);
            filmRepository.save(film);
            for (Actor actor : actors) {
                actorRepository.save(actor);
            }
            tx.commit();
        } catch (EntityRepositoryException e) {
            try {
                tx.rollback();
            } catch (EntityRepositoryException ex) {
                ex.printStackTrace();
            }
            throw new RuntimeException(e);
        } finally {
            try {
                tx.endTransaction();
            } catch (EntityRepositoryException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveFilms(List<Film> films) {
        EntityTransaction tx = new EntityTransaction();
        try {
            tx.initTransaction(filmRepository);
            for (Film film : films) {
                filmRepository.save(film);
            }
            tx.commit();
        } catch (EntityRepositoryException e) {
            try {
                tx.rollback();
            } catch (EntityRepositoryException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                tx.endTransaction();
            } catch (EntityRepositoryException e) {
                e.printStackTrace();
            }
        }
    }
}
