package by.shag.lesson37.mapping;

import by.shag.lesson37.jpa.model.Film;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FilmMapper implements EntityMapper<Film> {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String YEAR = "year";
    private static final String RAITING = "raiting";

    @Override
    public Film map(ResultSet resultSet) throws SQLException {
        Film film = new Film();
        film.setId(resultSet.getInt(ID));
        film.setName(resultSet.getString(NAME));
        film.setYear(resultSet.getInt(YEAR));
        film.setRating(resultSet.getDouble(RAITING));
        return film;
    }

    @Override
    public void populate(Film model, PreparedStatement statement) throws SQLException{
        statement.setString(1, model.getName());
        statement.setInt(2, model.getYear());
        statement.setDouble(3, model.getRating());
    }
}
