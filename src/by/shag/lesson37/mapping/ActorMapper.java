package by.shag.lesson37.mapping;

import by.shag.lesson37.jpa.model.Actor;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ActorMapper implements EntityMapper<Actor> {

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String LAST_NAME = "last_name";
    public static final String DATE_OF_BIRTH = "date_of_birth";

    @Override
    public Actor map(ResultSet resultSet) throws SQLException {
        Actor actor = new Actor();
        actor.setId(resultSet.getLong(ID));
        actor.setName(resultSet.getString(NAME));
        actor.setLastName(resultSet.getString(LAST_NAME));
        actor.setDateOfBirth(resultSet.getDate(DATE_OF_BIRTH));
        return actor;
    }

    @Override
    public void populate(Actor model, PreparedStatement statement) throws SQLException {
        statement.setString(1, model.getName());
        statement.setString(2, model.getLastName());
        statement.setDate(3, new Date(model.getDateOfBirth().getTime()));
    }
}
