package by.shag.lesson37;

import by.shag.lesson37.jpa.model.Actor;
import by.shag.lesson37.jpa.model.Film;
import by.shag.lesson37.service.FilmService;

import java.time.Instant;
import java.util.Date;
import java.util.List;

public class Runner {
    public static void main(String[] args) {

        Film film = new Film();
        film.setName("123");
        film.setYear(2021);
        film.setRating(5.5);

        Actor actor = new Actor();
        actor.setName("Vlad");
        actor.setLastName("Fuck");
        actor.setDateOfBirth(Date.from(Instant.now()));

        FilmService filmService = new FilmService();
        filmService.saveWithActors(film, List.of(actor));

    }
}
