package by.shag.lesson26;

import java.util.Arrays;

public class Runner {
    public static void main(String[] args)  throws InterruptedException {

        Thread worker = new WorkerThreat();
//        worker.setDaemon(true);

        Thread sleeper = new SleeperThreat();
//        sleeper.setDaemon(true);

        worker.start();
        sleeper.start();

        Thread.sleep(1_000L);

//        worker.interrupt();
//        sleeper.interrupt();

        worker.join(1_000);
        sleeper.join(2_000);

//        thread();
//        generate10Threat();
        System.out.println("Hello from main threat");
    }

    private static void thread() {

        Thread thread = Thread.currentThread();
        System.out.println(thread.getName());
        System.out.println(thread.getId());
        System.out.println(thread.getThreadGroup());
        Arrays.stream(thread.getStackTrace()).forEach(System.out::println);
        System.out.println(thread.getPriority());
        System.out.println(thread.getState());
        System.out.println(thread.isDaemon());
        System.out.println(thread.isAlive());
        System.out.println(thread.isInterrupted());
    }

    private static void generate10Threat() {

        for (int i = 0; i < 10; i++) {

//            Thread thread = new HelloWorldThreat();
//            thread.start();

//            Thread thread = new Thread(new HelloWorldRunnable());
            Thread thread = new Thread(() -> System.out.println(Thread.currentThread().getName()));
            thread.start();

        }

        System.out.println("Hello from main threat");
    }
}
