package by.shag.lesson26.test;

public class Runner {
    public static void main(String[] args) throws InterruptedException {
        Account account = new Account(10_000);
        System.out.println("Start balance = " + account.getBalanceInEuro());

        Thread withdrawThreat = new WithdrawThreat(account);
        Thread depositThreat = new DepositThreat(account);

        withdrawThreat.start();
        Thread.sleep(1_000L);
        depositThreat.start();

        withdrawThreat.join();
        depositThreat.join();

        System.out.println("End balance " + account.getBalanceInEuro());
    }
}
