package by.shag.lesson26.test;

public class WithdrawThreat extends Thread {

    private final Account account;

    public WithdrawThreat(Account account) {
        this.account = account;
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            try {
                account.waitAndWithdraw(5_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
