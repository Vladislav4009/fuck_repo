package by.shag.lesson26.test;

public class Account {

    private long balanceInEuro;

    public Account() {
    }

    public Account(long balanceInEuro) {
        this.balanceInEuro = balanceInEuro;
    }

    public long getBalanceInEuro() {
        return balanceInEuro;
    }

    public void setBalanceInEuro(long balanceInEuro) {
        this.balanceInEuro = balanceInEuro;
    }

    public synchronized void deposit (long amount) throws InterruptedException{
        balanceInEuro += amount;
        notify();
    }

    public synchronized void withdraw(long amount) {
        if (balanceInEuro < amount) {
            throw new NotEnoughMoneyException("not enough money for operation");
        }
        synchronized (this) {
            balanceInEuro -= amount;
        }
    }

    public synchronized void waitAndWithdraw(long amount) throws InterruptedException{
        while (balanceInEuro < amount) {
            System.out.println("Balance = " + balanceInEuro + " is lees than you have!");
            notify();
            wait();
        }
        balanceInEuro -= amount;
        System.out.println("Списали со счета " + amount + " Текущий баланс = " + balanceInEuro);
    }
}
