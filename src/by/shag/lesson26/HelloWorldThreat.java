package by.shag.lesson26;

public class HelloWorldThreat extends Thread {

    @Override
    public void run() {
        System.out.print("Hello");
        System.out.print(" world");
        System.out.print(" from");
        System.out.println(" threat " + getName());
    }
}
