package by.shag.lesson26;

public class WorkerThreat extends Thread {

    @Override
    public void run() {
        long sum = 0;
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            sum += i;
            if (i % 100 == 0 && isInterrupted()) {
                System.out.println("Loop is interrupter at i = " + i);
                break;
            }
        }
        System.out.println("worker threat is finished");
    }
}
