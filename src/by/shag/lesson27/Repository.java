package by.shag.lesson27;

public class Repository {

    public static final Integer MAX_CAPACITY = 10;

    private volatile int currentProductQuantity;  // берет лонг атомарно

    public Repository(int currentProductQuantity) {
        this.currentProductQuantity = currentProductQuantity;
    }

    public synchronized void getProduct() throws InterruptedException {
        while (currentProductQuantity < 1) {
            System.out.println("Склад пуст, ждем.");
            wait();
        }
        currentProductQuantity--;
        System.out.println("Купили 1 товар");
        System.out.println("Остаток на складе - " + currentProductQuantity);
        notifyAll();
    }

    public synchronized void produceProduct() throws InterruptedException {
        while (currentProductQuantity >= MAX_CAPACITY) {
            System.out.println("Склад полон");
            wait();
        }
        currentProductQuantity++;
        System.out.println("Сделали 1 товар");
        System.out.println("Товаров на складе - " + currentProductQuantity);
        notifyAll();
    }
}
