package by.shag.lesson27;

public class Runner {
    public static void main(String[] args) {
        Repository repository = new Repository(5);

        for (int i = 0; i < 100; i++) {
            new ConsumerThreat(repository).start();
        }
        for (int i = 0; i < 100; i++) {
            new ProducerThreat(repository).start();
        }
    }
}
