package by.shag.lesson27;

public class ConsumerThreat extends Thread {

    private Repository repository;

    public ConsumerThreat(Repository repository) {
        this.repository = repository;
    }

    @Override
    public void run() {
        try {
            repository.getProduct();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
