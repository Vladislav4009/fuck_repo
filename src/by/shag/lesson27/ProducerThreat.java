package by.shag.lesson27;

public class ProducerThreat extends Thread {

    private Repository repository;

    public ProducerThreat(Repository repository) {
        this.repository = repository;
    }

    @Override
    public void run() {
        try {
            repository.produceProduct();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
